class Convertidor{

    valor1=68;
    valor2=95;
    valor3=25;

    resultado;

    gra2rad(){

        this.resultado= this.valor1*Math.PI/180;
        return this.resultado;
    }

    ftcm(){

        this.resultado= (this.valor2*30.48);
        return this.resultado;
    }

    cel1far(){

        this.resultado= ((this.valor3) * 9/5)+32;
        return this.resultado;
    }

}

class Contestador{
    
    usuario;
    saludo;

    nombre(){

        this.saludo = (this.usuario);
        return this.saludo;
    }
}
